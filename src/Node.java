
import java.io.CharArrayReader;
import java.io.CharArrayWriter;
import java.util.*;

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n){
      this(n, null, null);
   }
   Node (String n, Node d, Node r) {
      name = n;
      firstChild = d;
      nextSibling = r;
   }
   
   public static Node parsePostfix (String s) {
      if(s.length() == 0) { throw new RuntimeException("NodeTree is empty!"); } // kui string on tühi ja puu puudub
      int bracketIndex = s.lastIndexOf(")"); // leiame viimase sulu indeks
      String name = s.substring(bracketIndex + 1); // kõige välima puuosa nimi on peale viimase sulu

      // kontrollime nime ning teeme peamist puu antud nimega
      if(name.contains(",") || name.contains(" ") || name.contains("\t")) { throw new RuntimeException("NodeTree " + s + " has invalid name"); }
      Node mainNode = new Node(name);

      // kui lapsed on
      if(s.contains(")")){

         // kui sulge on rohkem vajalikust ning alumisel elemendil puudub vanem
         if(s.substring(bracketIndex-1, bracketIndex + 1).equals("))")
                 && s.substring(0, 2).equals("((")) {
            throw new RuntimeException("NodeTree " + s + " has no first child!");
         }
         // võtame välistes suludes olevaid elemente
         String nextMember = s.substring(1, bracketIndex);
         Node intermediateNode = null; // teeme puu
         int depth = 0; // vaatame mis sügavisel element praegu on
         StringBuilder nextNodeBuilder = new StringBuilder();
         try {
            for (int i = 0; i < nextMember.length(); i++) {  // iga tähe vaatamine
               // kui element on viimane, siis võtame sõne lõpuni elemendid, teisel juhul järgmise indeksini element
               String currentChar = i + 1 == nextMember.length() ? nextMember.substring(i) : nextMember.substring(i, i + 1);

               // vaatame kui sügaval oleme
               if(currentChar.equals("(")){
                  depth++;
               }else if(currentChar.equals(")")){
                  depth--;
               }
               // kui on koma ja on esimene laps, ning SBsse on juba kirjutatud elemendid
               else if(currentChar.equals(",") && depth == 0 && nextNodeBuilder.length() > 0){
                  // kui selle elemendi puud veel pole - teeme
                  if(intermediateNode == null){
                     // saadame rekursiooni kõik eelmised elemendid, nt. ((A,B)C,D)K puust saadame rekursiooni (A,B)C
                     // ning kirjutame esimest last peamise puusse
                     intermediateNode = parsePostfix(nextNodeBuilder.toString());
                     mainNode.firstChild = intermediateNode;
                  }else{
                     // kui esimene puu on, siis lisame talle juurde "õe"-puu ning määrame viimaseks vaadatud lapseks seda puud
                     intermediateNode.nextSibling = parsePostfix(nextNodeBuilder.toString());
                     intermediateNode = intermediateNode.nextSibling;
                  }
                  // kustutame töödeldud puud tehes uue SB
                  nextNodeBuilder = new StringBuilder();
                  continue;
               }
               // kui komad ei tulnud - täiendame SBd tähega
               nextNodeBuilder.append(currentChar);
            }
            // kui puus on üks laps, siis täiendame sellega peamist puud
            if(intermediateNode == null){
               mainNode.firstChild = parsePostfix(nextNodeBuilder.toString());
            }
            // kui rohkem ühest - lisame viimasele töödeldud lapsele "õe"-puud
            else{
               intermediateNode.nextSibling = parsePostfix(nextNodeBuilder.toString());
            }
         }catch (RuntimeException e){
            throw new RuntimeException("Node " + s + " string is incorrect!"); // tekkib mingi probleem
         }
      }
      return mainNode;
   }

   public String leftParentheticRepresentation() {
      StringBuilder nodeString = new StringBuilder();
      nodeString.append(name);
      Node children = firstChild;
      if(children != null){ // kui laps on siis lisame ( sõnesse
         nodeString.append("(");
         while (children != null){ // kuni lapsi on võetud puu oksal, täiendame sõne
            if(!nodeString.substring(nodeString.length() - 1).equals("(")){
               nodeString.append(",");
            }
            nodeString.append(children.leftParentheticRepresentation()); // rekursioonis tegeleme lastega
            children = children.nextSibling; // järgmisena võtame "õe"-puu lapsi
         }
         nodeString.append(")"); // kui lapsi pole - paneme sulud
      }
      return nodeString.toString();
   }

   public static void main (String[] param) {
      /*String s1 = "((((E)D)C)B)A";
      Node t3 = Node.parsePostfix (s1);
      String r = t3.leftParentheticRepresentation();
      System.out.println(r);*/
      Node t5 = Node.parsePostfix("A1,G5");
      String s = "(B1(D, E),C)A";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
      Node tri = Node.parsePostfix("B1");
      System.out.println(tri.leftParentheticRepresentation());
   }
}

